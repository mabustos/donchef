#DonChef

Don Chef is a web based app that wants you learn to cook in a simple and fun way.

##TODO:
* Build and design Frontend.
* Add and improve exception handlers.
* Add security layer by restricting access to API endpoints.
* Add property encryptor.

##Requirements:
* Java 8
* Maven

##How to run?
* Create a MySQL database, and provide connection URL in application.properties (once applicaiton is started, hibernate will populate it with tables).
* Run mvn spring-boot:run

