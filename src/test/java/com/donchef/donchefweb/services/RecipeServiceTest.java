package com.donchef.donchefweb.services;

import com.donchef.donchefweb.model.entities.Ingredient;
import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import com.donchef.donchefweb.util.handlers.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)

@SpringBootTest
public class RecipeServiceTest {

    @Autowired
    RecipeService recipeService;

    @Autowired
    IngredientService ingredientService;

    private Ingredient water,sugar,milk,flour;
    private RecipeStep recipeStep1, recipeStep2;
    private Recipe recipe1, recipe2;

    @Before
    public void setUp() {

        water = new Ingredient();
        water.setId(1l);
        water.setName("Water");

        sugar = new Ingredient();
        sugar.setId(2l);
        sugar.setName("Sugar");

        milk = new Ingredient();
        milk.setId(3l);
        milk.setName("Milk");

        flour = new Ingredient();
        flour.setId(4l);
        flour.setName("Flour");

        water = ingredientService.insertIngredient(water);
        sugar = ingredientService.insertIngredient(sugar);
        milk = ingredientService.insertIngredient(milk);
        flour = ingredientService.insertIngredient(flour);

        recipeStep1 = new RecipeStep();
        recipeStep1.setId(1l);
        recipeStep1.setName("Recipe step 1");
        recipeStep1.setDescription("Stir the boil water while you spread some sugar on it.");
        recipeStep1.setTime(60);
        recipeStep1.addIngredient(water);
        recipeStep1.addIngredient(sugar);

        recipeStep2 = new RecipeStep();
        recipeStep2.setId(2l);
        recipeStep2.setName("Recipe step 2");
        recipeStep2.setDescription("Mix the flour and milk into a smooth paste");
        recipeStep2.setTime(120);
        recipeStep2.addIngredient(milk);
        recipeStep2.addIngredient(flour);

        recipe1 = new Recipe();
        recipe1.setId(1l);
        recipe1.setName("Luis XVII konditorei secret recipe");
        recipe1.setDescription("This is the full recipe.");
        recipe1.addStep(recipeStep1);

        recipe2 = new Recipe();
        recipe2.setId(2l);
        recipe2.setName("Half Luis XVII konditorei secret recipe");
        recipe2.setDescription("This is half recipe.");
        recipe2.addStep(recipeStep2);


    }

    @Test
    @Transactional
    public void shouldObtainEmptyWhenRepositoryIsEmpty() {
        List<Recipe> recipes = recipeService.findAllRecipes();
        assertThat(recipes.size()).isEqualTo(0);
    }

    @Test
    @Transactional
    public void shouldStoreARecipe(){
        Recipe recipe = recipeService.insertRecipe(recipe2);
        List<Recipe> recipes = recipeService.findAllRecipes();
        assertThat(recipes.size()).isEqualTo(1);
    }

    @Test
    @Transactional
    public void shouldInsertNestedRecipeSteps(){
        Recipe recipe = recipeService.insertRecipe(recipe1);
        assertThat(recipe.getSteps()).hasSize(1);
    }

    @Test
    @Transactional
    public void shouldUpdateRecipeSteps(){
        Recipe recipe = recipeService.insertRecipe(recipe1);
        recipe.addStep(recipeStep2);
        recipe = recipeService.updateRecipe(recipe);
        assertThat(recipe.getSteps()).hasSize(2);
    }

    @Test(expected = ResourceNotFoundException.class)
    @Transactional
    public void shouldRemoveRecipe(){
        Recipe recipe = recipeService.insertRecipe(recipe1);
        recipeService.removeRecipe(recipe.getId());
        recipeService.findRecipe(recipe.getId());
    }


}
