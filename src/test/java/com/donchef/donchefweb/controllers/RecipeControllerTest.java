package com.donchef.donchefweb.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.donchef.donchefweb.model.entities.Ingredient;
import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import com.donchef.donchefweb.model.repositories.IngredientRepository;
import com.donchef.donchefweb.model.repositories.RecipeRepository;
import com.donchef.donchefweb.model.repositories.RecipeStepRepository;
import com.donchef.donchefweb.services.IngredientService;
import com.donchef.donchefweb.services.RecipeService;
import com.donchef.donchefweb.services.RecipeStepService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(SpringRunner.class)
public class RecipeControllerTest {
    @Mock
    private RecipeService recipeService;
    @InjectMocks
    private RecipeController recipeController;

    private MockMvc mockMvc;

    private Ingredient water,sugar,milk,flour;
    private RecipeStep recipeStep1, recipeStep2;
    private Recipe recipe1, recipe2;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(recipeController).build();

        water = new Ingredient();
        water.setId(1l);
        water.setName("Water");

        sugar = new Ingredient();
        sugar.setId(2l);
        sugar.setName("Sugar");

        milk = new Ingredient();
        milk.setId(3l);
        milk.setName("Milk");

        flour = new Ingredient();
        flour.setId(4l);
        flour.setName("Flour");


        recipeStep1 = new RecipeStep();
        recipeStep1.setId(1l);
        recipeStep1.setName("Recipe step 1");
        recipeStep1.setDescription("Stir the boil water while you spread some sugar on it.");
        recipeStep1.setTime(60);
        recipeStep1.addIngredient(water);
        recipeStep1.addIngredient(sugar);


        recipeStep2 = new RecipeStep();
        recipeStep2.setId(2l);
        recipeStep2.setName("Recipe step 2");
        recipeStep2.setDescription("Mix the flour and milk into a smooth paste.");
        recipeStep2.setTime(120);
        recipeStep2.addIngredient(milk);
        recipeStep2.addIngredient(flour);


        recipe1 = new Recipe();
        recipe1.setId(1l);
        recipe1.setName("Luis XVII konditorei secret recipe");
        recipe1.setDescription("This is the first half recipe.");
        recipe1.addStep(recipeStep1);

        recipe2 = new Recipe();
        recipe2.setId(2l);
        recipe2.setName("Half Luis XVII konditorei secret recipe");
        recipe2.setDescription("This is the second half recipe.");
        recipe2.addStep(recipeStep2);

    }

    @Test
    public void canObtainRecipes() throws Exception {
        given(recipeService.findRecipe(recipe1.getId())).willReturn(recipe1);
        given(recipeService.findRecipe(recipe2.getId())).willReturn(recipe2);

        mockMvc.perform(
                get("/api/recipes/"+recipe1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        org.hamcrest.Matchers.comparesEqualTo(
                                "{\"id\":1," +
                                        "\"name\":\"Luis XVII konditorei secret recipe\"," +
                                        "\"description\":\"This is the first half recipe.\"," +
                                        "\"steps\":" +
                                            "[{\"id\":1," +
                                            "\"name\":\"Recipe step 1\"," +
                                            "\"description\":\"Stir the boil water while you spread some sugar on it.\"," +
                                            "\"time\":60," +
                                            "\"stepRequirements\":null," +
                                            "\"ingredients\":" +
                                                "[{\"id\":1,\"name\":\"Water\"}" +
                                                ",{\"id\":2,\"name\":\"Sugar\"}]" +
                                        "}]}"
                        ))
                );

        mockMvc.perform(
                get("/api/recipes/"+recipe2.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        org.hamcrest.Matchers.comparesEqualTo(
                                "{\"id\":2," +
                                        "\"name\":\"Half Luis XVII konditorei secret recipe\"," +
                                        "\"description\":\"This is the second half recipe.\"," +
                                        "\"steps\":" +
                                            "[{\"id\":2," +
                                            "\"name\":\"Recipe step 2\"," +
                                            "\"description\":\"Mix the flour and milk into a smooth paste.\"," +
                                            "\"time\":120," +
                                            "\"stepRequirements\":null," +
                                            "\"ingredients\":" +
                                                "[{\"id\":3,\"name\":\"Milk\"}" +
                                                ",{\"id\":4,\"name\":\"Flour\"}]" +
                                        "}]}"
                        ))
                );
    }

    @Test
    public void canObtainRecipeStepsOutFromRecipe() throws Exception {
        given(recipeService.findAllRecipeSteps(recipe1.getId())).willReturn(recipe1.getSteps());

        mockMvc.perform(
                get("/api/recipes/"+recipe1.getId()+"/steps"))
                .andExpect(status().isOk())
                .andExpect(content().string(
                        org.hamcrest.Matchers.comparesEqualTo(
                                "[{\"id\":1," +
                                        "\"name\":\"Recipe step 1\"," +
                                        "\"description\":\"Stir the boil water while you spread some sugar on it.\"," +
                                        "\"time\":60," +
                                        "\"stepRequirements\":null," +
                                        "\"ingredients\":" +
                                            "[{\"id\":1,\"name\":\"Water\"}" +
                                            ",{\"id\":2,\"name\":\"Sugar\"}]"+
                                        "}]"
                        ))
                );
    }



}