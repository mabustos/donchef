package com.donchef.donchefweb.services;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;

import java.util.List;

public interface RecipeService {
    Recipe findRecipe(Long id);
    List<Recipe> findAllRecipes();
    List<RecipeStep> findAllRecipeSteps(Long id);
    Recipe insertRecipe(Recipe recipe);
    Recipe updateRecipe(Recipe recipe);
    void removeRecipe(Long id);
}
