package com.donchef.donchefweb.services;

import com.donchef.donchefweb.model.entities.Ingredient;

import java.util.List;

public interface IngredientService {
    Ingredient findIngredient(Long id);
    List<Ingredient> findAllIngredients();
    Ingredient insertIngredient(Ingredient ingredient);
    Ingredient updateIngredient(Ingredient ingredient);
    void removeIngredient(Long id);
}
