package com.donchef.donchefweb.services.implementations;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import com.donchef.donchefweb.model.repositories.RecipeStepRepository;
import com.donchef.donchefweb.services.RecipeStepService;
import com.donchef.donchefweb.util.handlers.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeStepServiceImpl implements RecipeStepService {

    @Autowired
    private RecipeStepRepository recipeStepRepository;

    @Override
    public RecipeStep findRecipeStep(Long id) {
        Optional<RecipeStep> recipe = recipeStepRepository.findById(id);
        if(recipe.isPresent()){
            return recipe.get();
        }
        else{
            throw new ResourceNotFoundException(id, "recipe step not found");
        }
    }

    @Override
    public List<RecipeStep> findAllRecipeSteps() {
        return recipeStepRepository.findAll();
    }

    @Override
    public RecipeStep insertRecipeStep(RecipeStep recipeStep) {
        recipeStep.setId(0l);
        return recipeStepRepository.save(recipeStep);
    }

    @Override
    public RecipeStep updateRecipeStep(RecipeStep recipeStep) {
        Optional<RecipeStep> recipe = recipeStepRepository.findById(recipeStep.getId());
        if(recipe.isPresent()){
            return recipeStepRepository.save(recipeStep);
        }
        else{
            throw new ResourceNotFoundException(recipeStep.getId(), "recipe step not found");
        }
    }

    @Override
    public void removeRecipeStep(Long id) {
        Optional<RecipeStep> recipe = recipeStepRepository.findById(id);
        if(recipe.isPresent()){
            recipeStepRepository.deleteById(id);
        }
        else{
            throw new ResourceNotFoundException(id, "recipe step not found");
        }
    }
}
