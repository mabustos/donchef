package com.donchef.donchefweb.services.implementations;

import com.donchef.donchefweb.model.entities.Ingredient;
import com.donchef.donchefweb.model.repositories.IngredientRepository;
import com.donchef.donchefweb.services.IngredientService;
import com.donchef.donchefweb.util.handlers.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IngredientServiceImpl implements IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public Ingredient findIngredient(Long id) {
        Optional<Ingredient> ingredient = ingredientRepository.findById(id);
        if(ingredient.isPresent()){
            return ingredient.get();
        }
        else{
            throw new ResourceNotFoundException(id, "ingredient not found");
        }
    }

    @Override
    public List<Ingredient> findAllIngredients() {
        return ingredientRepository.findAll();
    }

    @Override
    public Ingredient insertIngredient(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }

    @Override
    public Ingredient updateIngredient(Ingredient ingredient) {
        Optional<Ingredient> ingredientRetrieved = ingredientRepository.findById(ingredient.getId());
        if(ingredientRetrieved.isPresent()){
            return ingredientRepository.save(ingredient);
        }
        else{
            throw new ResourceNotFoundException(ingredient.getId(), "ingredient not found");
        }
    }

    @Override
    public void removeIngredient(Long id) {
        Optional<Ingredient> ingredientRetrieved = ingredientRepository.findById(id);
        if(ingredientRetrieved.isPresent()){
            ingredientRepository.deleteById(id);
        }
        else{
            throw new ResourceNotFoundException(id, "ingredient not found");
        }
    }
}
