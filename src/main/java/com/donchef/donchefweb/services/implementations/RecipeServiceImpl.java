package com.donchef.donchefweb.services.implementations;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import com.donchef.donchefweb.model.repositories.RecipeRepository;
import com.donchef.donchefweb.services.RecipeService;
import com.donchef.donchefweb.util.handlers.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Override
    public Recipe findRecipe(Long id) throws EntityNotFoundException{
        Optional<Recipe> recipe = recipeRepository.findById(id);
        if(recipe.isPresent()){
            return recipe.get();
        }
        else{
            throw new ResourceNotFoundException(id, "recipe not found");
        }
    }

    @Override
    public List<Recipe> findAllRecipes() {
        return recipeRepository.findAll();
    }

    @Override
    public List<RecipeStep> findAllRecipeSteps(Long id) {
        Optional<Recipe> recipe = recipeRepository.findById(id);
        if(recipe.isPresent()){
            return recipe.get().getSteps();
        }
        else{
            throw new ResourceNotFoundException(id, "recipe not found");
        }
    }

    @Override
    public Recipe insertRecipe(Recipe recipe) {
        //recipe.setId(0l);

        return recipeRepository.save(recipe);
    }

    @Override
    public Recipe updateRecipe(Recipe recipe) {
        Optional<Recipe> requestedRecipe = recipeRepository.findById(recipe.getId());
        if(requestedRecipe.isPresent()){
            return recipeRepository.save(recipe);
        }
        else{
            throw new ResourceNotFoundException(recipe.getId(), "recipe not found");
        }
    }

    @Override
    public void removeRecipe(Long id) {
        Optional<Recipe> requestedRecipe = recipeRepository.findById(id);
        if(requestedRecipe.isPresent()){
            recipeRepository.deleteById(id);
        }
        else{
            throw new ResourceNotFoundException(id, "recipe not found");
        }
    }
}
