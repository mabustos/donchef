package com.donchef.donchefweb.services;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;

import java.util.List;

public interface RecipeStepService {
    RecipeStep findRecipeStep(Long id);
    List<RecipeStep> findAllRecipeSteps();
    //List<RecipeStep> findByRecipe(long recipeId);
    RecipeStep insertRecipeStep(RecipeStep recipeStep);
    RecipeStep updateRecipeStep(RecipeStep recipeStep);
    void removeRecipeStep(Long id);
}
