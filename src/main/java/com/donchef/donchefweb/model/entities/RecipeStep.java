package com.donchef.donchefweb.model.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class RecipeStep implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    private String name;

    private String description;

    private int time;

    @OneToMany
    @JoinColumn(name = "id")
    private List<RecipeStep> stepRequirements;

    @ManyToMany
    @JoinColumn(name = "id")
    private List<Ingredient> ingredients;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<RecipeStep> getStepRequirements() {
        return stepRequirements;
    }

    public void setStepRequirements(ArrayList<RecipeStep> stepRequirements) {
        this.stepRequirements = stepRequirements;
    }

    public void addStepRequirement(RecipeStep recipeStep){
        if(this.stepRequirements==null){
            this.stepRequirements = new ArrayList<>();
        }
        this.stepRequirements.add(recipeStep);
    }

    public void setStepRequirements(List<RecipeStep> stepRequirements) {
        this.stepRequirements = stepRequirements;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredient(Ingredient ingredient){
        if(this.ingredients==null)
            this.ingredients = new ArrayList<>();
        this.ingredients.add(ingredient);
    }

    @Override
    public String toString() {
        return "RecipeStep{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", time=" + time +
                ", stepRequirements=" + stepRequirements +
                ", ingredients=" + ingredients +
                '}';
    }
}
