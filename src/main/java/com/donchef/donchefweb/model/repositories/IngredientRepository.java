package com.donchef.donchefweb.model.repositories;

import com.donchef.donchefweb.model.entities.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {
}
