package com.donchef.donchefweb.model.repositories;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<RecipeStep> findBySteps_id(long id);
}
