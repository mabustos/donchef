package com.donchef.donchefweb.controllers;

import com.donchef.donchefweb.model.entities.Recipe;
import com.donchef.donchefweb.model.entities.RecipeStep;
import com.donchef.donchefweb.services.RecipeService;
import com.donchef.donchefweb.services.RecipeStepService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/recipes")
public class RecipeController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RecipeController.class);

    @Autowired
    private RecipeService recipeService;
    @Autowired
    private RecipeStepService recipeStepService;

    // Recipe related API endpoints

    @GetMapping("/")
    List<Recipe> getAllRecipes(){
        return recipeService.findAllRecipes();
    }

    @GetMapping("/{recipeId}")
    Recipe getRecipeById(@PathVariable("recipeId") long id){
        return recipeService.findRecipe(id);
    }

    @PostMapping("/")
    Recipe createRecipe(@Valid @RequestBody Recipe recipe){
        return recipeService.insertRecipe(recipe);
    }

    @PutMapping("/")
    Recipe updateRecipe(@Valid @RequestBody Recipe recipe){
        return recipeService.updateRecipe(recipe);
    }

    @DeleteMapping("/{recipeId}")
    void deleteRecipe(@PathVariable("recipeId") long recipeId){
        recipeService.removeRecipe(recipeId);
    }

    // RecipeSteps related API endpoints

    @GetMapping("/{recipeId}/steps")
    List<RecipeStep> getAllRecipeStepsByRecipeId(@PathVariable("recipeId") long recipeId){
        return recipeService.findAllRecipeSteps(recipeId);
    }

    @GetMapping("/{recipeId}/steps/{recipeStepId}")
    RecipeStep getRecipeStepByRecipeIdAndRecipeStepId(@PathVariable("recipeId") long recipeId, @PathVariable("recipeStepId") long recipeStepId){
        return recipeStepService.findRecipeStep(recipeStepId);
    }

    @PostMapping("/{recipeId}/steps")
    RecipeStep createRecipeStep(@PathVariable("recipeId") long recipeId, @Valid @RequestBody RecipeStep recipeStep){
        recipeStep.setId(0l);
        return recipeStepService.insertRecipeStep(recipeStep);
    }

    @PutMapping("/{recipeId}/steps/")
    RecipeStep updateRecipeStep(@PathVariable("recipeId") long recipeId, @Valid @RequestBody RecipeStep recipeStep){
        return recipeStepService.updateRecipeStep(recipeStep);
    }

    @DeleteMapping("/{recipeId}/steps/{recipeStepId}")
    void deleteRecipeStep(@PathVariable("recipeId") long recipeId, @PathVariable("recipeStepId") long recipeStepId){
        recipeStepService.removeRecipeStep(recipeStepId);
    }
}
