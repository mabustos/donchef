package com.donchef.donchefweb.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MainController.class);

    @GetMapping
    void index(){

    }
}
