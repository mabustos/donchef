package com.donchef.donchefweb.controllers;

import com.donchef.donchefweb.model.entities.Ingredient;
import com.donchef.donchefweb.services.IngredientService;
import com.donchef.donchefweb.services.implementations.IngredientServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/ingredients")
public class IngredientController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(IngredientController.class);
    @Autowired
    private IngredientService ingredientService;

    @GetMapping("/")
    List<Ingredient> getAllRecipes(){
        return ingredientService.findAllIngredients();
    }

    @GetMapping("/{ingredientId}")
    Ingredient getRecipeById(@PathVariable("ingredientId") long id){
        return ingredientService.findIngredient(id);
    }

    @PostMapping("/")
    Ingredient createRecipe(@Valid @RequestBody Ingredient ingredient){
        ingredient.setId(0l);
        return ingredientService.insertIngredient(ingredient);
    }

    @PutMapping("/")
    Ingredient updateRecipe(@Valid @RequestBody Ingredient ingredient){
        return ingredientService.updateIngredient(ingredient);
    }

    @DeleteMapping("/{ingredientId}")
    void deleteRecipe(@PathVariable("ingredientId") long id){
        ingredientService.removeIngredient(id);
    }

}
