package com.donchef.donchefweb.util.handlers;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

/*
Recovered from https://github.com/damithme/spring-boot-REST/blob/master/spring-boot-part3/src/main/java/com/mydevgeek/util/ValidationUtil.java
 */
public class ValidationUtil {

    public static List<String> fromBindingErrors(Errors errors) {
        List<String> validErrors = new ArrayList<String>();
        for (ObjectError objectError : errors.getAllErrors()) {
            validErrors.add(objectError.getDefaultMessage());
        }
        return validErrors;
    }
}
