package com.donchef.donchefweb.util.handlers;

// Todo: Handle this exception in ExceptionHandlerController
public class ResourceNotFoundException extends RuntimeException {
    private Long resourceId;

    public ResourceNotFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}
