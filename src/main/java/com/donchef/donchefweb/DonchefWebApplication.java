package com.donchef.donchefweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DonchefWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DonchefWebApplication.class, args);
	}
}
